package com.strings;

import java.util.Scanner;

public class Students {
    String[] Students=new String[20];
    int size;
    public void initializeStringArray(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the size:- ");
        size=scanner.nextInt();
        Students=new String[size];
        System.out.println("Enter Names:- ");
        for (int iterate = 0; iterate < size; iterate++) {
            Students[iterate]=scanner.next();
        }
    }
    public void display(){
        for (int iterate = 0; iterate < size; iterate++) {
            System.out.println(Students[iterate]);
        }
    }
}
